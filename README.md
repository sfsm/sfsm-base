# Static finite state machine

Contains proc macros used by the [sfsm](https://gitlab.com/sfsm/sfsm) crate. Head over there for a more detailed description of the project.